import json
from datetime import datetime
from collections import defaultdict
from operator import itemgetter


def request_json(file):
    with open(file, "r") as f:
        data = json.load(f)
    return data


def transform_experience_list(data):
    def convert_date_from_iso_format(iso_format):
        return datetime.fromisoformat(iso_format).date()

    def create_dict_dates(start_date, end_date):
        temp_dict = {
            "start_date": convert_date_from_iso_format(start_date),
            "end_date": convert_date_from_iso_format(end_date),
        }
        return temp_dict

    temp_list = []
    experience_list = data["freelance"]["professionalExperiences"]
    for el in experience_list:
        dict_dates = create_dict_dates(el["startDate"], el["endDate"])
        for sk in el["skills"]:
            sk.update(dict_dates)
            temp_list.append(sk)
    return temp_list


def transform_experience_dict(experiencies):
    temp_dict = defaultdict(list)
    for cs in experiencies:
        key = f"{cs['id']}_{cs['name']}"
        temp_dict[key].append(cs["start_date"])
        temp_dict[key].append(cs["end_date"])
    return temp_dict


def transform_structured_skills(experiencies_dict):
    temp_list = []
    for k, v in experiencies_dict.items():
        temporary_dict = {}
        id, name = k.split("_")
        temporary_dict["id"] = int(id)
        temporary_dict["name"] = name
        temporary_dict["durationInMonths"] = (max(v) - min(v)) // 30
        temp_list.append(temporary_dict)
    return temp_list


def calculate_day(structured_skills):
    for item in structured_skills:
        item["durationInMonths"] = int(item["durationInMonths"].days)


def sort_items_by_id(structured_skills):
    return sorted(structured_skills, key=itemgetter("id"))


def format_response(data, computed_skills):
    context = {}
    context["freelance"] = {}
    context["freelance"]["id"] = data["freelance"]["id"]
    context["freelance"]["computedSkills"] = computed_skills
    return context


def main():
    data = request_json("examples/freelancer.json")
    experiencies = transform_experience_list(data)
    experiencies_dict = transform_experience_dict(experiencies)
    structured_skills = transform_structured_skills(experiencies_dict)
    calculate_day(structured_skills)
    computed_skills = sort_items_by_id(structured_skills)
    response = format_response(data, computed_skills)

    print(response)


if __name__ == "__main__":
    main()
